from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
# Create your models here.
class Post(models.Model):
    title=models.CharField(max_length=64)
    content=models.TextField()
    date_posted=models.DateTimeField(default=timezone.now)
    author=models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('post-detail',kwargs={'pk':self.pk})

        
    
    
class Like(models.Model):
    post=models.ForeignKey(Post,on_delete=models.CASCADE)
    user=models.ManyToManyField(User)
    counter=models.IntegerField(default=0)
    like_dated=models.DateTimeField(default=timezone.now)


    def __str__(self):
        return f'post like {self.post.title},{self.counter}'

class Comment(models.Model):
    post=models.ForeignKey(Post,related_name='comments',on_delete=models.CASCADE)
    user=models.ForeignKey(User,related_name='comments',on_delete=models.CASCADE)
    parent=models.ForeignKey('self',on_delete=models.CASCADE,null=True)
    content=models.TextField()
    timestam=models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f'post comment by  {self.user.username},{self.post.title}'   
