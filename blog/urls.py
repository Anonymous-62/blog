from django.urls import path
from . import views
from .views import PostListView,PostDetailView,PostCreateView,PostUpdateView,PostDeleteView,UserPostListView

urlpatterns=[
    path('',views.PostListView.as_view(),name="blog-home"),
    path('user/<str:username>', UserPostListView.as_view(), name='user-posts'),
    path('post/<int:pk>/',views.PostDetailView.as_view(),name="post-detail"),
    path('post/new/',views.PostCreateView.as_view(),name="post-create"),
    path('post/<int:pk>/update/',views.PostUpdateView.as_view(),name="post-update"),
    path('post/<int:pk>/delete/',views.PostDeleteView.as_view(),name="post-delete"),
    path('about/',views.about,name="blog-about"),
    path('ajax/postlike/<int:pk>/',views.PostLike,name="post-like"),
    path('ajax/postcomment/<int:pk>/',views.PostComment,name="post-comment"),
    path('ajax/commentupdate/<int:pk>/',views.CommentUpdate,name="comment-update"),
    path('ajax/commentdelete/<int:pk>/',views.CommentDelete,name="comment-delete"),
]