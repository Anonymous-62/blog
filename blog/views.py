from django.shortcuts import render,get_object_or_404
from django.contrib.auth.models import User
from .models import Post,Like,Comment
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin,UserPassesTestMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)

# Create your views here.
def home(request):
    context={
        'posts':Post.objects.all()
    }
    return render(request,'blog/home.html',context)
class PostListView(ListView):
    model=Post
    template_name='blog/home.html'
    # context_object_name='posts'
    ordering=['-date_posted']
    paginate_by = 5


class PostDetailView(DetailView):
    model=Post
    context_object_name='post'

class PostCreateView(LoginRequiredMixin,CreateView):
    model=Post
    fields=['title','content']
    
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

class PostUpdateView(LoginRequiredMixin,UserPassesTestMixin,UpdateView):
    model=Post
    fields=['title','content']
    
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False

class PostDeleteView(LoginRequiredMixin,UserPassesTestMixin,DeleteView):
    model=Post
    success_url='/'
    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False

class UserPostListView(ListView):
    model=Post
    template_name='blog/user_posts.html'
    context_object_name='posts'
    paginate_by = 5

    def get_queryset(self):
        user=get_object_or_404(User,username=self.kwargs.get('username'))
        return Post.objects.filter(author=user).order_by('-date_posted')
    

        
def about(request):
    return render(request,'blog/about.html',{'title':'about'})

def PostLike(request,pk):
    like_post=Like.objects.filter(
        user__username=request.user,post_id=pk
    ).first()
    if like_post:
        like_post.delete()
        likes=Like.objects.filter(post_id=pk)
        total_likes=sum((like.counter) for like in likes)
        return JsonResponse({'total_likes':total_likes})
    
    else:
        post=Post.objects.get(pk=pk)
        like=Like(post=post,counter=1)
        like.save()
        user=User.objects.get(username=request.user)
        like.user.add(user)
        likes=Like.objects.filter(post_id=pk)
        total_likes=sum((like.counter) for like in likes)
        return JsonResponse({'total_likes':total_likes})
    
def PostComment(request,pk):
    data=dict()
    print('dfgh')
    post=get_object_or_404(Post,pk=pk)
    if request.method=='POST':
        content=request.POST.get('comment')
        print(content)
        user=User.objects.get(username=request.user)
        if post and content and user:
            comment=Comment(post=post,user=user,content=content)
            comment.save()
    post_comments=Comment.objects.filter(post=post).order_by('-timestam')
    data['comments']=render_to_string('blog/partial__comments.html',context={'post':post_comments},request=request)
    return JsonResponse(data)
    
def CommentUpdate(request,pk):
    comment=Comment.objects.get(pk=pk)
    try:
        data=dict()
        print("comment",comment)
        if request.method=='POST':
            content=request.POST.get('content')
            print(content)
            comment.content=content
            comment.save()
    except Exception as error:
        print(error)
    data['update_comment']=render_to_string('blog/update_comment.html',context={'content':comment},request=request)
    return JsonResponse(data)

def CommentDelete(request,pk):
    comment=Comment.objects.get(pk=pk)
    data=dict()
    tag=request.GET.get('tag')
    if tag == 'delete':
        comment.delete()
        return JsonResponse({'status':'comment delete succesully'})
    data['comment_delete']=render_to_string('blog/delete_comment.html',context={'content':comment},request=request)
    data['status']='ok'
    return JsonResponse(data)